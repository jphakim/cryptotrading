package johnhakim.cryptotrading.Adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;

import johnhakim.cryptotrading.Model.OrderBook;
import johnhakim.cryptotrading.R;

/**
 * Created by johnp on 9/2/2017.
 */

public class OrderBookAdapter extends RecyclerView.Adapter<OrderBookAdapter.OrderBookHolder> {
    OrderBook mOrderBook;

    private static String TAG = "OrderBookAdapter";

    public OrderBookAdapter(OrderBook orderBook) {
        mOrderBook = orderBook;
    }

    @Override
    public OrderBookHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_view, parent, false);
        return new OrderBookHolder(view);

    }

    @Override
    public void onBindViewHolder(OrderBookHolder holder, int position) {
        DecimalFormat df = new DecimalFormat("#.00");
        holder.bids.setText(mOrderBook.getBids().get(position).get(0));
        holder.amount.setText(mOrderBook.getBids().get(position).get(1));
        holder.ask.setText(mOrderBook.getAsks().get(position).get(0));
        holder.askAmount.setText(mOrderBook.getAsks().get(position).get(1));
        double bid =  Double.valueOf(mOrderBook.getBids().get(position).get(0));
        double amount =  Double.valueOf(mOrderBook.getBids().get(position).get(1));
        double sum = bid * amount;
        double ask = Double.valueOf(mOrderBook.getAsks().get(position).get(0));
        double askAmount = Double.valueOf((mOrderBook.getAsks().get(position).get(1)));
        double sum2 = ask * askAmount;
        holder.value.setText(df.format(sum));
        holder.askValue.setText(df.format(sum2));
    }

    @Override
    public int getItemCount() {
        Log.i(TAG, "getItemCount: " + mOrderBook.getBids().size());
        return mOrderBook.getBids().size();
    }

    class OrderBookHolder extends RecyclerView.ViewHolder{
        private TextView bids,amount,value,ask,askAmount,askValue;
        public OrderBookHolder(View itemView) {
            super(itemView);
            bids = (TextView)itemView.findViewById(R.id.bid);
            amount = (TextView)itemView.findViewById(R.id.amount);
            value = (TextView)itemView.findViewById(R.id.value);
            ask = itemView.findViewById(R.id.ask);
            askAmount = itemView.findViewById(R.id.ask_amount);
            askValue = itemView.findViewById(R.id.ask_value);
        }
    }
}
