package johnhakim.cryptotrading.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.firebase.jobdispatcher.JobParameters;

import johnhakim.cryptotrading.Activity.MainActivity;
import johnhakim.cryptotrading.Model.PriceAlert;
import johnhakim.cryptotrading.R;
import johnhakim.cryptotrading.rest.TransactionClient;
import johnhakim.cryptotrading.rest.TransactionService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by johnp on 9/6/2017.
 */

public class JobService extends com.firebase.jobdispatcher.JobService {
    private static String TAG = "JobService";

    @Override
    public boolean onStartJob(JobParameters job) {
        final SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        String v = prefs.getString("pricealert","5000");
        Log.i(TAG, "onStartJob: called");
        Toast.makeText(this,"Called",Toast.LENGTH_SHORT).show();
        callData(v);
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        return false;
    }

    private void callData(final String value) {

        if ( value!= "" ) {

            TransactionService transactionService = TransactionClient.getClient().create(TransactionService.class);
            PriceAlert priceAlert;
            Call<PriceAlert> call = transactionService.getTickerHour();
            call.enqueue(new Callback<PriceAlert>() {
                @Override
                public void onResponse(Call<PriceAlert> call, Response<PriceAlert> response) {
                    PriceAlert priceAlert = response.body();
                    if (priceAlert!=null) {
                        if (Double.valueOf(priceAlert.getLast()) < Double.valueOf(value)) {
                            createNotification();

                        }
                    }

                }

                @Override
                public void onFailure(Call<PriceAlert> call, Throwable t) {

                }
            });
        }
    }

    private void createNotification(){
        Intent intent = new Intent(this, MainActivity.class);
        int requestID = (int) System.currentTimeMillis(); //unique requestID to differentiate between various notification with same NotifId
        int flags = PendingIntent.FLAG_CANCEL_CURRENT;
        PendingIntent pIntent = PendingIntent.getActivity(this, requestID, intent, flags);
        NotificationCompat.Builder mBuilder =

                new NotificationCompat.Builder(getApplication())
                        .setSmallIcon(R.drawable.ic_pricechange)
                        .setContentTitle("Bitcoin value dropped!")
                        .setContentText("Bitcoin has fell below your value")
                        .setAutoCancel(true)
                        .setContentIntent(pIntent);

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(0, mBuilder.build());
    }
}
