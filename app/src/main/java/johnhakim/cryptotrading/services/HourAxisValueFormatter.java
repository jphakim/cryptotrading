package com.johnhakim.cryptotrading;

import android.util.Log;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.content.ContentValues.TAG;

/**
 * Created by johnp on 9/4/2017.
 */


public class HourAxisValueFormatter implements IAxisValueFormatter {


    private long referenceTimestamp; // minimum timestamp in your data set
    private DateFormat mDataFormat;
    private Date mDate;
    ArrayList<String> xValues;

    public HourAxisValueFormatter(ArrayList xvalues) {
        xValues = xvalues;
    }



    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        Log.i(TAG, "getFormattedValue: ");
        return xValues.get((int) value % xValues.size());
    }


}

