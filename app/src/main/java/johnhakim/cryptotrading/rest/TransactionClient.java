package johnhakim.cryptotrading.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by johnp on 8/31/2017.
 */

public class TransactionClient {
    public static final String BASE_URL = " https://www.bitstamp.net/";

    private static Retrofit sRetrofit = null;

    public static Retrofit getClient(){
        if (sRetrofit == null){
            sRetrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return sRetrofit;
    }
}
