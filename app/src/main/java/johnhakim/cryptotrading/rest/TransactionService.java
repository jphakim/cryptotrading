package johnhakim.cryptotrading.rest;


import java.util.List;

import johnhakim.cryptotrading.Model.OrderBook;
import johnhakim.cryptotrading.Model.PriceAlert;
import johnhakim.cryptotrading.Model.Transactions;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by johnp on 8/31/2017.
 */

public interface TransactionService {
    @GET("api/v2/transactions/btcusd/")
    Call<List<Transactions>> getTransactions();
    @GET("api/v2/order_book/btcusd/")
    Call <OrderBook> getOrderBook();
    @GET("api/v2/ticker_hour/btcusd/")
    Call<PriceAlert> getTickerHour();


}
