package johnhakim.cryptotrading.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import johnhakim.cryptotrading.Adapters.OrderBookAdapter;
import johnhakim.cryptotrading.Model.OrderBook;
import johnhakim.cryptotrading.R;
import johnhakim.cryptotrading.rest.TransactionClient;
import johnhakim.cryptotrading.rest.TransactionService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by johnp on 9/8/2017.
 */

public class OrderBookFragment extends Fragment {

    private static String TAG = "OrderBookFragment";
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private EditText mEditText;
    private final OrderBook mOrderBook = null;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//      setRetainInstance(true);
        Log.i(TAG, "onCreate: started");
        callData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.orderbook_layout,container,false);
        mRecyclerView = (RecyclerView)v.findViewById(R.id.orderbook_recycler_view);
        mSwipeRefreshLayout = (SwipeRefreshLayout)v.findViewById(R.id.swiper);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callData();
                mRecyclerView.invalidate();
                if(mSwipeRefreshLayout.isRefreshing()){
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        });
        return v;
    }

    private void callData(){
        Log.i(TAG, "callData: called");
        TransactionService transactionService = TransactionClient.getClient().create(TransactionService.class);
        Call<OrderBook> call = transactionService.getOrderBook();
        call.enqueue(new Callback<OrderBook>() {
            @Override
            public void onResponse(Call<OrderBook> call, Response<OrderBook> response) {
                OrderBook orderbook = response.body();
                orderbook.getBids().size();
                Log.i(TAG, "onResponse: Downloaded ");
                mRecyclerView.setAdapter(new OrderBookAdapter(orderbook));
                mRecyclerView.invalidate();
            }

            @Override
            public void onFailure(Call<OrderBook> call, Throwable t) {
                Log.e(TAG, "onFailure: data not downloaded ",t );

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy: Called");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop: called");
    }
}
