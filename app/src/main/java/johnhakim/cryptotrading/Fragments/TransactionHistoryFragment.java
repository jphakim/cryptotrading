package johnhakim.cryptotrading.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.johnhakim.cryptotrading.HourAxisValueFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import johnhakim.cryptotrading.Model.Transactions;
import johnhakim.cryptotrading.R;
import johnhakim.cryptotrading.rest.TransactionClient;
import johnhakim.cryptotrading.rest.TransactionService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by johnp on 9/8/2017.
 */

public class TransactionHistoryFragment extends Fragment {
    private LineChart chart;

    private static  final String  TAG = "TransactionHistory";

    private List<Entry> mEntries = new ArrayList<Entry>();
    private Context mContext;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setRetainInstance(true);
        callData();
        Log.i(TAG, "onCreate: started");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.chart_view,container,false);
        chart = (LineChart) v.findViewById(R.id.chart);


        return v;
    }

    private void callData(){
        final TransactionService transactionClient = TransactionClient.getClient().create(TransactionService.class);
        Call<List<Transactions>> call = transactionClient.getTransactions();
        call.enqueue(new Callback<List<Transactions>>() {
            @Override
            public void onResponse(Call<List<Transactions>> call, Response<List<Transactions>> response) {
                List<Transactions> transactionHistory = response.body();

                ArrayList<String> xValues = new ArrayList<String>();

                for(int i = 0; i < transactionHistory.size();i++){
                    mEntries.add(new Entry(transactionHistory.get(i).getDate(),transactionHistory.get(i).getPrice()));
                    Log.i(TAG, "onResponse: called date " + transactionHistory.get(i).getDate());
                    //convert unix time to date time and add to array
                    long unixSeconds = Long.parseLong(transactionHistory.get(i).getStringDate());
                    Date date = new Date(unixSeconds*1000L);
                    SimpleDateFormat sdf = new SimpleDateFormat("h:mm a");
                    sdf.setTimeZone(TimeZone.getDefault());
                    String formattedDate = sdf.format(date);

                    xValues.add(formattedDate);


                }
                LineDataSet dataSet = new LineDataSet(mEntries,"History");
                LineData lineData = new LineData(dataSet);
                dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                dataSet.setDrawFilled(true);
                chart.setTouchEnabled(true);
                chart.setData(lineData);
                chart.setPinchZoom(true);
                chart.setMaxVisibleValueCount(4);
                chart.setHighlightPerTapEnabled(true);
                chart.setAutoScaleMinMaxEnabled(true);
                Log.i(TAG, "onResponse: date " + mEntries.get(mEntries.size()-1).getX() );
                chart.setVisibleXRangeMaximum(2);
                chart.getXAxis().setValueFormatter(new HourAxisValueFormatter(xValues));
                YAxis yAxis = chart.getAxisLeft();
                yAxis.setDrawGridLines(false);
                XAxis xAxis = chart.getXAxis();
                xAxis.setDrawGridLines(false);
                xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                xAxis.setTextSize(10f);
                chart.fitScreen();
                chart.setHighlightPerTapEnabled(true);
                chart.invalidate(); // refresh
                Log.i(TAG, "onCreate: started chart");
            }
            @Override
            public void onFailure(Call<List<Transactions>> call, Throwable t) {
            }
        });
    }
}
